package plt;

public class Translator {
	
	public static final String NIL = "nil";
	
	private String phrase;

	public Translator(String inputPhrase) {
		phrase=inputPhrase;
	}

	public String getPhrase() {
		
		return phrase;
	}

	public String translate() {
		
		String[] splitstr;
		String translation="";
		String divide=" ";
		
		String checkstr="";
	 if(!(phrase.isEmpty())) {
		 splitstr=splitPhrase(phrase);
		 int length=splitstr.length;
		 if(phrase.contains(" ")) {
				divide=" ";
			}else if(phrase.contains("-")) {
				divide="-";
			}
		 for(int counter=0;counter<length;counter++) {
			 if(counter>0) {
					translation=translation.concat(divide);
			}
			 if(startWithVowel(splitstr[counter])) {
					if(splitstr[counter].endsWith("y")) {
						checkstr=splitstr[counter].concat("nay");
						translation=translation.concat(splitPunctuations(checkstr));
					}
					else if(endWithVowel(splitstr[counter])) {
						checkstr=splitstr[counter].concat("yay");
						translation=translation.concat(splitPunctuations(checkstr));
					}
					else if(!endWithVowel(splitstr[counter])) {
						checkstr=splitstr[counter].concat("ay");
						translation=translation.concat(splitPunctuations(checkstr));
					}
				}else {
					int cns=countConsonant(splitstr[counter]);
					checkstr=splitstr[counter].substring(cns).concat( splitstr[counter].substring(0,cns).concat("ay"));
					translation=translation.concat(splitPunctuations(checkstr));
				}
		 }
		return translation;	
	 }
		return NIL;
	}
	
	
	
	private boolean startWithVowel(String phrase) {
		return phrase.startsWith("a") || phrase.startsWith("e") || phrase.startsWith("i") || phrase.startsWith("o") || phrase.startsWith("u"); 
		
	}
	
	private boolean endWithVowel(String phrase) {
		return phrase.endsWith("a") || phrase.endsWith("e") || phrase.endsWith("i") || phrase.endsWith("o") || phrase.endsWith("u"); 
		
	}
	
	private boolean containsPunctuation(String phrase) {
		return phrase.contains(".") || phrase.contains(",") || phrase.contains(";") || phrase.contains(":") || phrase.contains("?")  || phrase.contains("!") 
				|| phrase.contains("'") || phrase.contains("(") || phrase.contains(")");
	}
	
	private int countConsonant(String phrase) {
		int i=0;
		
		do {	
			
			i++;
		}while(!(startWithVowel(phrase.substring(i))));
		return i;
	}
	
	private String[] splitPhrase(String phrase) {
			
			String[] str;
			if(phrase.contains(" ")) {
				str=phrase.split(" ");
			}else {
				str=phrase.split("-");
			}
			
			return str;
			
		}
	
	private String splitPunctuations(String phrase) {
		String phrasePunct="";
		if(containsPunctuation(phrase)) {
			int position=findPunctuations(phrase);
			String punctuation=phrase.substring(position,position+1);
			phrase=phrase.replace(punctuation, "");
			phrasePunct=phrase+punctuation;
			return phrasePunct;
			
		}
		return phrase;
	}
	
	private int findPunctuations(String phrase) {
		int position=0;
		if(phrase.contains("!")) {
			position=phrase.indexOf("!");
		}else if(phrase.contains(".")) {
			position=phrase.indexOf(".");
		}else if(phrase.contains(",")) {
			position=phrase.indexOf(",");
		}else if(phrase.contains(";")) {
			position=phrase.indexOf(";");
		}else if(phrase.contains(":")) {
			position=phrase.indexOf(":");
		}else if(phrase.contains("?")) {
			position=phrase.indexOf("?");
		}else if(phrase.contains("'")) {
			position=phrase.indexOf("'");
		}else if(phrase.contains("(")) {
			position=phrase.indexOf("(");
		}else if(phrase.contains(")")) {
			position=phrase.indexOf(")");
		}
		
		return position;
	}

}
