package plt;

import static org.junit.Assert.*;

import org.junit.Test;

public class TranslatorTest {

	@Test
	public void testInputPhrase() {
		String inputPhrase="hello world";
		Translator translator = new Translator(inputPhrase);
		assertEquals("hello world", translator.getPhrase());
		
	}
	
	@Test
	public void testTranslationNullPhrase() {
		String inputPhrase="";
		Translator translator = new Translator(inputPhrase);
		assertEquals(Translator.NIL, translator.translate());
		
	}
	
	@Test
	public void testTranslationPhraseStartingWithAEndingWithY() {
		String inputPhrase="any";
		Translator translator = new Translator(inputPhrase);
		assertEquals("anynay", translator.translate());
		
	}
	
	@Test
	public void testTranslationPhraseStartingWithUEndingWithY() {
		String inputPhrase="utility";
		Translator translator = new Translator(inputPhrase);
		assertEquals("utilitynay", translator.translate());
		
	}
	
	@Test
	public void testTranslationPhraseStartWithVowelEndingWithVowel() {
		String inputPhrase="apple";
		Translator translator = new Translator(inputPhrase);
		assertEquals("appleyay", translator.translate());
		
	}
	
	@Test
	public void testTranslationPhraseStartWithVowelEndingWithConsonant() {
		String inputPhrase="ask";
		Translator translator = new Translator(inputPhrase);
		assertEquals("askay", translator.translate());
		
	}
	
	@Test
	public void testTranslationPhraseStartingWithConsonant() {
		String inputPhrase="hello";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay", translator.translate());
		
	}
	
	@Test
	public void testTranslationPhraseStartingWithMoreConsonant() {
		String inputPhrase="known";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ownknay", translator.translate());
		
	}
	
	@Test
	public void testPhraseWithSpace() {
		String inputPhrase = "hello world";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay orldway", translator.translate());
		
	}
	
	@Test
	public void testPhraseWithDash() {
		String inputPhrase = "well-being";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellway-eingbay", translator.translate());
		
	}
	
	@Test
	public void testPhraseWithPunctuations() {
		String inputPhrase = "hello world!";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay orldway!", translator.translate());
		
	}

}
